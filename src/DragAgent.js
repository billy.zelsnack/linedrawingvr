

DragAgent = function()
{
	this.gripDragVertex=null;
	this.gripDragEdge=null;
	this.currentEdge=null;

	this.begin=function(editor)
	{
		this.gripDragVertex=null;
		this.gripDragEdge=null;

		const snapval=0.02;

		var tipPosition=editor.getControllerTipPosition(editor.hands[0]);

		//-- try to find a vertex to drag around
		for( var ii=0; ii<editor.vertexes.length; ii++ )
		{
			var dist=tipPosition.distanceTo(editor.vertexes[ii].position);
			if( dist<snapval )
			{
				this.gripDragVertex=editor.vertexes[ii];
			}
		}

		//-- don't try to drag anything else if you're already dragging a vertex
		if( this.gripDragVertex!=null )
		{
			return;
		}
		
		//-- try to find an edge to drag around
		for( var ii=0; ii<editor.edges.length; ii++ )
		{
			var dist=tipPosition.distanceTo(editor.edges[ii].position);
			if( dist<snapval )
			{
				this.gripDragEdge=editor.edges[ii];
			}
		}
		
	}

	this.end=function(editor)
	{
		if(this.gripDragVertex!=null)
		{
			var tipPosition=editor.getControllerTipPosition(editor.hands[0]);
			var foundEdge=editor.findEdge(tipPosition);
			if(foundEdge!=null)
			{
				editor.splitedge(foundEdge);
			}

			var foundVertex=editor.findVertex(this.gripDragVertex.position,this.gripDragVertex);
			if(foundVertex!=null)
			{
				for( ii=0; ii<editor.edges.length; ii++ )
				{
					var edge=editor.edges[ii];
					if(edge.vertexa==this.gripDragVertex)
					{
						edge.vertexa=foundVertex;
						editor.alignedge(edge);
					}
					if(edge.vertexb==this.gripDragVertex)
					{
						edge.vertexb=foundVertex;
						editor.alignedge(edge);
					}
				}
			}
		}

		this.gripDragVertex=null;
		this.gripDragEdge=null;
	}

	this.update=function(editor)
	{
		var tipPosition=editor.getControllerTipPosition(editor.hands[0]);

		if(this.gripDragVertex!=null)
		{
			this.gripDragVertex.position.copy(tipPosition);

			for( var ii=0; ii<editor.edges.length; ii++ )
			{
				editor.alignedge( editor.edges[ii] );
			}
		}

		if(this.gripDragEdge!=null)
		{
			var delta=this.gripDragEdge.position.clone().sub(tipPosition);

			for( var ii=0; ii<editor.vertexes.length; ii++ )
			{
				editor.vertexes[ii].position.sub(delta);
			}

			for( var ii=0; ii<editor.edges.length; ii++ )
			{
				editor.edges[ii].position.sub(delta);
			}
		}

	}

}
