
http://billy.zelsnack.gitlab.io/linedrawingvr/ to try it out.

Only tested on the Rift in WebVR versions of Chrome and Firefox.

Right controller trigger alone to drag out a line.
Right controller trigger+grip to add a new line while drawing.
Right controller grip on a vertex to drag the vertex.
Right controller grip on an edge center to drag the drawing.

Drawing and dragging work with vertex and center of edge snapping.
Dragging an edge center will drag then entire object.

Currently redundant vertices are not cleaned up and litter the place up.

Third-person mouse OrbitControls work when not in VR mode.

Attributions (todo:links)
three.js library
VRController.js
threex.minecraft.js
3dbenchy.stl


