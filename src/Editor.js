
//import {THREEx} from "../src/threex.minecraft.js"
//import {THREEx} from "./threex.minecraft.js"

//require( 'THREEx' );


var lineTool=new LineTool();


Editor=function()
{
    this.vertexes=[];
    this.edges=[];
    this.edgeRadius=0.005*0.75;
    this.vertexRadius=0.01*0.75;
    this.hands=[null,null];
    
    this.scene=new THREE.Scene();

	/*
	this.floor = new THREE.Mesh(
        new THREE.BoxGeometry( 6, 0.2, 6, 6, 1, 6 ),
        new THREE.MeshStandardMaterial({ roughness: 1.0, metalness: 0.0, color: 0xF0F0F0, wireframe: false })
    );
    this.floor.position.y = -0.1
	this.scene.add( this.floor )
	*/
	
	var plane = new THREE.Mesh(
		new THREE.PlaneBufferGeometry( 40, 40 ),
		new THREE.MeshPhongMaterial( { color: 0x999999, specular: 0x101010 } )
	);
	plane.rotation.x = -Math.PI/2;
	//plane.position.y = -0.5;
	this.scene.add( plane );
	plane.receiveShadow = true;
    
	/*
	this.light = new THREE.DirectionalLight()
    this.light.position.set(  1, 1.5, -0.5 )
	this.scene.add( this.light )
	*/

	this.scene.add( new THREE.HemisphereLight( 0x909090, 0x404040 ))


	
			// Light
			this.light = new THREE.DirectionalLight( 0xFFFFFF );
			this.light.position.set( 2.5, 5, 2.5 );
			this.light.target.position.copy( this.scene.position );
			this.light.castShadow = true;
			this.light.shadow.camera.left = -2;
			this.light.shadow.camera.top = -2;
			this.light.shadow.camera.right = 2;
			this.light.shadow.camera.bottom = 2;
			this.light.shadow.camera.near = 0.2;
			this.light.shadow.camera.far = 10;
			this.light.shadow.bias = -.001
			this.light.shadow.mapSize.width = this.light.shadow.mapSize.height = 2048;
			//this.light.shadowDarkness = .7;
			this.scene.add( this.light );
	
			directionalLightHelper=new THREE.DirectionalLightHelper(this.light,1);
			this.scene.add(directionalLightHelper);
	


	var loader = new THREE.STLLoader();
	loader.load( './models/3DBenchy_-_Dualprint_-_Hull_Box_Bridge_walls_Rod-holder_Chimney_v02_-_3DBenchy.com.stl', ( geometry ) => {

		var material = new THREE.MeshPhongMaterial( { color: 0xff5533, specular: 0x111111, shininess: 200 } );
		var mesh = new THREE.Mesh( geometry, material );

		mesh.position.set( 0, 0, 0 );
		mesh.rotation.set( - Math.PI / 2, 0 , 0 );
		mesh.scale.set( 0.05, 0.05, 0.05 );

		mesh.castShadow = true;
		mesh.receiveShadow = true;

		this.scene.add( mesh );

	} );

	var loader = new THREE.STLLoader();
	loader.load( './models/3DBenchy_-_Dualprint_-_Gunwale_Deck_Plate_Wheel_Frames_Roof_Chimney_top_-_3DBenchy.com.stl', ( geometry ) => {

		var material = new THREE.MeshPhongMaterial( { color: 0x3355ff, specular: 0x111111, shininess: 200 } );
		var mesh = new THREE.Mesh( geometry, material );

		mesh.position.set( 0, 0, 0 );
		mesh.rotation.set( - Math.PI / 2, 0 , 0 );
		mesh.scale.set( 0.05, 0.05, 0.05 );

		mesh.castShadow = true;
		mesh.receiveShadow = true;

		this.scene.add( mesh );

	} );




//var cameraBase = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.01, 100 );
//editor.scene.add(cameraBase);

var cameraHead = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.01, 100 );
this.scene.add(cameraHead);
var cameraHeadMaterial = new THREE.MeshStandardMaterial({ color: 0xFF4040, flatShading: THREE.FlatShading });
var cameraHeadMesh = new THREE.Mesh( new THREE.CylinderGeometry( 0.005, 0.05, 0.1, 10 ), cameraHeadMaterial );
cameraHeadMesh.rotation.x = -Math.PI / 2;
cameraHead.add(cameraHeadMesh)
//cameraHead.helper=new THREE.CameraHelper( cameraHead );
//editor.scene.add( cameraHead.helper );







var cameraThird = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.01, 100 );
cameraThird.position.y=1.8;
this.scene.add(cameraThird);
var cameraThirdMaterial = new THREE.MeshStandardMaterial({ color: 0x0000FF, flatShading: THREE.FlatShading });
var cameraThirdMesh = new THREE.Mesh( new THREE.CylinderGeometry( 0.005, 0.05, 0.1, 10 ), cameraHeadMaterial );
cameraThirdMesh.rotation.x = -Math.PI / 2;
cameraThird.add(cameraThirdMesh);
cameraThird.helper=new THREE.CameraHelper( cameraThird );
this.scene.add( cameraThird.helper );
var cameraThirdControls=new THREE.OrbitControls( cameraThird );
cameraThirdControls.enabled=true;
cameraThirdControls.rotateSpeed = 0.2;
cameraThirdControls.zoomSpeed = 1;
cameraThirdControls.panSpeed = 1;



let screenRenderTarget = new THREE.WebGLRenderTarget( 512, 512, { format: THREE.RGBFormat } );
let screenGeometry = new THREE.PlaneBufferGeometry( 1, 1 );
let screenMaterial = new THREE.MeshBasicMaterial( );//{map:screenRenderTarget.texture} );
screenMaterial.map=screenRenderTarget.texture;
let screen = new THREE.Mesh( screenGeometry, screenMaterial );
screen.position.set(0,1,-1);
this.scene.add(screen);


var character	= new THREEx.MinecraftChar('./images/char.png');
character.root.remove(character.headGroup);
character.headGroup.rotation.set(0,Math.PI,0);
character.headGroup.position.set(0,-0.1,0);
cameraHead.add(character.headGroup);
character.root.rotation.y=Math.PI;
this.scene.add(character.root);


var characterThird	= new THREEx.MinecraftChar('./images/char.png');
characterThird.root.remove(characterThird.headGroup);
characterThird.headGroup.rotation.set(0,Math.PI,0);
characterThird.headGroup.position.set(0,-0.1,0);
cameraThird.add(characterThird.headGroup);
characterThird.root.rotation.y=Math.PI;
this.scene.add(characterThird.root);






    

    this.controllerConnected=function(controller)
    {
    
        var controllerMaterial = new THREE.MeshStandardMaterial({ color: 0xFF4040, flatShading: THREE.FlatShading });
        var controllerMesh = new THREE.Mesh( new THREE.CylinderGeometry( 0.005, 0.05, 0.1, 10 ), controllerMaterial );
        var handleMesh = new THREE.Mesh( new THREE.BoxGeometry( 0.03, 0.1, 0.03 ), controllerMaterial );
    
        this.scene.add( controller );
    
        controllerMesh.rotation.x = -Math.PI / 2;
        handleMesh.position.y = -0.05;
        controllerMesh.add( handleMesh );
        controller.userData.mesh = controllerMesh//  So we can change the color later.
        controller.add( controllerMesh );
    
        if( controller.gamepad.hand==="left" )
        {
            this.hands[1]=controller;
        }
        else
        if( controller.gamepad.hand==="right" )
        {
            this.hands[0]=controller;
        }
    
        if( this.hands[0]!=null && this.hands[1]!=null )
        {
            lineTool.attach(this);
        }
    
    }

    this.update=function()
    {
		if( lineTool.editor!=null )
        lineTool.update();        
	}
	

	this.render=function(renderer)
	{
		renderer.vr.getCamera(cameraHead);
		var standingPosition = new THREE.Vector3();
		standingPosition.setFromMatrixPosition( renderer.vr.getStandingMatrix() );
		cameraHead.position.add( standingPosition );
	
		
		//character.headGroup.matrix.copy(cameraHead.localMatrix);
		//character.headGroup.ma
	
		{
		character.root.position.copy(cameraHead.position);
		character.root.position.y-=0.9;
		var dirHead=character.headGroup.getWorldDirection();
		var dirBody=character.root.getWorldDirection();
		character.root.position.x-=dirBody.x*0.1;
		character.root.position.z-=dirBody.z*0.1;
		dirBody.lerp(dirHead,0.05);
		character.root.lookAt( new THREE.Vector3(character.root.position.x+dirBody.x,character.root.position.y,character.root.position.z+dirBody.z) );
		}
	
		{
			characterThird.root.position.copy(cameraThird.position);
			characterThird.root.position.y-=0.9;
			var dirHead=characterThird.headGroup.getWorldDirection();
			var dirBody=characterThird.root.getWorldDirection();
			characterThird.root.position.x-=dirBody.x*0.1;
			characterThird.root.position.z-=dirBody.z*0.1;
			dirBody.lerp(dirHead,0.05);
			characterThird.root.lookAt( new THREE.Vector3(characterThird.root.position.x+dirBody.x,characterThird.root.position.y,characterThird.root.position.z+dirBody.z) );
		}
		
		
	
	
		cameraThirdControls.update();
	
		cameraHead.visible=!renderer.vr.enabled;
		//cameraHead.helper.visible=!renderer.vr.enabled;
		cameraThird.visible=renderer.vr.enabled;
		cameraThird.helper.visible=renderer.vr.enabled;
	
		if( renderer.vr.enabled )
		{
			screen.visible=false;
			renderer.vr.enabled=false;
			renderer.render( this.scene, cameraThird, screenRenderTarget );
			renderer.vr.enabled=true;
			screen.visible=true;
			renderer.render( this.scene, cameraHead );
		}
		else
		{
			screen.visible=false;
			renderer.render( this.scene, cameraHead, screenRenderTarget );
			screen.visible=true;
			renderer.render( this.scene, cameraThird );
		}		
	
	}



     
    this.createVertexSphere=function( position )
    {
	    var geometry = new THREE.SphereBufferGeometry( this.vertexRadius, 10, 10 );
	    var material = new THREE.MeshStandardMaterial({ color: 0xFF4040, flatShading: THREE.FlatShading });
	    var mesh = new THREE.Mesh( geometry, material );

	    mesh.position.copy(position);
	    this.scene.add( mesh );

	    return mesh;
    }

    this.createVertexCube=function( position )
    {
	    var geometry = new THREE.BoxBufferGeometry( this.vertexRadius*1.5, this.vertexRadius*1.5, this.vertexRadius*1.5 );
	    var material = new THREE.MeshStandardMaterial({ color: 0xFF4040, flatShading: THREE.FlatShading });
	    var mesh = new THREE.Mesh( geometry, material );

	    mesh.position.x=position.x;
	    mesh.position.y=position.y;
	    mesh.position.z=position.z;
	    this.scene.add( mesh );

	    return mesh;
    }

    this.findVertex=function(position,filtervertex)
    {
	    const snapval=this.vertexRadius*2;

	    var mindist=snapval;
	    var vertex=null;
	    for( var ii=0; ii<this.vertexes.length; ii++ )
	    {
		    if(this.vertexes[ii]==filtervertex){continue;}

		    var dist=position.distanceTo(this.vertexes[ii].position);
		    if(dist<mindist)
		    {
			    vertex=this.vertexes[ii];
			    mindist=dist;
		    }
	    }
	
	    return vertex;
    }


    this.createVertex=function(position)
    {
    	vertex=this.createVertexSphere(position);
	    this.vertexes.push(vertex)
	    return vertex;	
    }

    this.findOrCreateVertex=function(position)
    {
	    var vertex=this.findVertex(position,null);
	    if(vertex==null)
	    {
		    vertex=this.createVertex(position);
	    }
	    return vertex;
    }

    this.findEdge=function(position)
    {
	    const snapval=this.vertexRadius*2;

	    var mindist=snapval;
	    var edge=null;
	    for( var ii=0; ii<this.edges.length; ii++ )
	    {
		    var dist=position.distanceTo(this.edges[ii].position);
		    if(dist<mindist)
		    {
			    edge=this.edges[ii];
			    mindist=dist;
		    }
	    }
	
	    return edge;
    }

    this.snap=function(position,filteredge,filtervertexc)
    {	
	    const snapval=this.vertexRadius*2;
	    var mindist=snapval;
	    var snappos=position.clone();

	    var snapped=false;

	    var filtervertexa=null;
	    var filtervertexb=null;
	    if(filteredge!=null)
	    {
		    filtervertexa=filteredge.vertexa;
		    filtervertexb=filteredge.vertexb;
	    }

	    for( var ii=0; ii<this.vertexes.length; ii++ )
	    {
		    if( this.vertexes[ii]==filtervertexa ){continue;}
		    if( this.vertexes[ii]==filtervertexb ){continue;}
		    if( this.vertexes[ii]==filtervertexc ){continue;}

		    var dist=position.distanceTo(this.vertexes[ii].position);
		    if( dist<mindist )
		    {
			    mindist=dist;
			    snappos=this.vertexes[ii].position;
			    snapped=true;
		    }

	    }
	
	    for( var ii=0; ii<this.edges.length; ii++ )
	    {
		    if( this.edges[ii]==filteredge ){continue;}

		    var dist=position.distanceTo(this.edges[ii].position);
		    if( dist<mindist )
		    {
			    mindist=dist;
			    snappos=this.edges[ii].position;
			    snapped=true;
		    }
	    }
	
	    position.copy(snappos);

	    return snapped;
    }

    this.splitedge=function(foundEdge)
    {
	    var geometry = new THREE.CylinderBufferGeometry( this.edgeRadius, this.edgeRadius, 1, 10 );
	    var material = new THREE.MeshStandardMaterial({ color: 0xFFFF00, flatShading: THREE.FlatShading });

	    var edge = new THREE.Mesh( geometry, material );
	    edge.vertexa=foundEdge.vertexa;
	    edge.vertexb=this.findOrCreateVertex(foundEdge.position);
	    edge.position=new THREE.Vector3().lerpVectors(edge.vertexa.position,edge.vertexb.position,0.5);
	    this.scene.add( edge );
	    this.edges.push(edge);
	    this.alignedge(edge);

	    foundEdge.vertexa=edge.vertexb;
	    foundEdge.position=new THREE.Vector3().lerpVectors(foundEdge.vertexa.position,foundEdge.vertexb.position,0.5);
	    this.alignedge(foundEdge);
    }

    this.alignedge=function( edge )
    {
	    var positiona=edge.vertexa.position;
	    var positionb=edge.vertexb.position;

	    var dir=positionb.clone().sub(positiona);
	    if( dir.lengthSq()<0.0001 ){ edge.scale.y=0.0001; return; }

	    edge.quaternion.setFromUnitVectors(new THREE.Vector3(0,1,0), dir.clone().normalize() );
	    edge.position.x=positiona.x+dir.x*0.5;
	    edge.position.y=positiona.y+dir.y*0.5;
	    edge.position.z=positiona.z+dir.z*0.5;
	    edge.scale.y=dir.length();
	    edge.update=true;
    }

    this.getControllerTipPosition=function(controller)
    {
    	var xAxis=new THREE.Vector3();
	    var yAxis=new THREE.Vector3();
	    var zAxis=new THREE.Vector3();
	    controller.matrixWorld.extractBasis(xAxis,yAxis,zAxis);

	    var standingPosition = new THREE.Vector3();
	    standingPosition.setFromMatrixPosition( controller.standingMatrix );

	    standingPosition.x+=controller.position.x-zAxis.x*0.05;
	    standingPosition.y+=controller.position.y-zAxis.y*0.05;
	    standingPosition.z+=controller.position.z-zAxis.z*0.05;

	    return standingPosition;
    }
}







