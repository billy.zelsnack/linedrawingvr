

LineAgent = function()
{
    this.triggerDown=false;
    this.currentEdge=null;
    this.snapVertex=null;

	this.begin=function(editor)
	{
		this.triggerDown=true;

		var tipPosition=editor.getControllerTipPosition(editor.hands[0]);
		
		var foundEdge=editor.findEdge(tipPosition);
		if(foundEdge!=null)
		{
			editor.splitedge(foundEdge);

			this.currentEdge=null;
			//return;
		}

		var snapped=editor.snap(tipPosition,null,null);

		var geometry = new THREE.CylinderBufferGeometry( editor.edgeRadius, editor.edgeRadius, 1, 10 );
		var material = new THREE.MeshStandardMaterial({ color: 0xFFFF00, flatShading: THREE.FlatShading });
		var edge = new THREE.Mesh( geometry, material );

		edge.position.copy(tipPosition);
		editor.scene.add( edge );

		edge.vertexa=editor.findOrCreateVertex(tipPosition);
		edge.vertexb=editor.createVertex(tipPosition);
		editor.edges.push(edge);
		editor.alignedge(edge);

		this.currentEdge=edge;
	}

	this.end=function(editor)        
    {
		this.triggerDown=false;

		var tipPosition=editor.getControllerTipPosition(editor.hands[0]);
		var foundEdge=editor.findEdge(tipPosition);
		if(foundEdge!=null)
		{
			editor.splitedge(foundEdge);
		}

		if( this.currentEdge!=null )
		{
			var foundVertex=editor.findVertex(this.currentEdge.vertexb.position,this.currentEdge.vertexb);
			if(foundVertex!=null)
			{
				this.currentEdge.vertexb=foundVertex;
				editor.alignedge(this.currentEdge);
			}
		}

	}

	this.update=function(editor)
	{      
        if( this.triggerDown && this.currentEdge!=null )
		{
            var tipPosition=editor.getControllerTipPosition(editor.hands[0]);            
			editor.snap(tipPosition,this.currentEdge,null);
			this.currentEdge.vertexb.position.copy(tipPosition);
			editor.alignedge( this.currentEdge );
        }       
	}

}

