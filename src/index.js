//import { Editor } from 'Editor';

var editor=new Editor();



var renderer = new THREE.WebGLRenderer( {antialias: true} );
renderer.setClearColor( 0x101010 );
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.vr.enabled = true;
renderer.vr.standing = true;
renderer.shadowMap.enabled = true;
renderer.shadowMapSoft = true;
document.body.appendChild( renderer.domElement );



WEBVR.checkAvailability().catch( function( message )
{
	document.body.appendChild( WEBVR.getMessageContainer( message ))
});

WEBVR.getVRDisplay( function( display )
{
	renderer.vr.setDevice( display );

	window.addEventListener( 'vrdisplaypresentchange', () =>
	{
		console.log("display.isPresenting "+display.isPresenting);
		renderer.vr.enabled=display.isPresenting;
	});

	document.body.appendChild( WEBVR.getButton( display, renderer.domElement ) );
});





window.addEventListener( 'resize', (event)=>
{
	console.log("onWindowResize");

	//cameraHead.aspect = window.innerWidth / window.innerHeight;
	//cameraHead.updateProjectionMatrix();

	//cameraThird.aspect = window.innerWidth / window.innerHeight;
	//cameraThird.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );
});

window.addEventListener( 'load', ()=> 
{ 
	console.log("window.onload"); 
	
	renderer.vr.enabled = false;
});



window.addEventListener( 'vr controller connected', (event)=>
{
	var controller = event.detail
	controller.standingMatrix = renderer.vr.getStandingMatrix();
	//controller.head = camera;//window.camera;

	console.log("vr controller connected "+controller.gamepad.hand);

	editor.controllerConnected(controller);
});

renderer.animate( ()=>
{
	THREE.VRController.update();
	editor.update();


	editor.render(renderer);
});

