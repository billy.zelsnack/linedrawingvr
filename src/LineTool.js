

//function LineTool()
LineTool = function()
{
	this.triggerDown=false;
	this.gripDown=false;
	this.editor=null;

	this.snapVertex=null;

	this.dragAgent=new DragAgent();
	this.lineAgent=new LineAgent();

	this.attach=function(editor)
	{
		console.log("LineTool.attach to editor");
		this.editor=editor;

		this.snapVertex=this.editor.createVertexCube(new THREE.Vector3(0,0,0));		

		this.editor.hands[0].addEventListener( 'trigger press began', this.triggerPressed.bind(this) );
		this.editor.hands[0].addEventListener( 'trigger press ended', this.triggerReleased.bind(this) );
		this.editor.hands[0].addEventListener( 'grip press began', this.gripPressed.bind(this) );
		this.editor.hands[0].addEventListener( 'grip press ended', this.gripReleased.bind(this) );
		this.editor.hands[0].addEventListener( 'A press began', this.aPressed.bind(this) );
	}

	this.triggerPressed=function(event)
	{
		this.triggerDown=true;

		this.lineAgent.begin(this.editor);
	}

	this.triggerReleased=function(event)
	{
		this.triggerDown=false;

		this.lineAgent.end(this.editor);
	}

	this.gripPressed=function(event)
	{
		if( this.triggerDown )
		{
			this.lineAgent.end(this.editor);
			this.lineAgent.begin(this.editor);
		}
		else
		{
			this.gripDown=true;
			this.dragAgent.begin(this.editor);
		}			
	}

	this.gripReleased=function(event)
	{
		if( !this.triggerDown )
		{	
			this.gripDown=false;
			this.dragAgent.end(this.editor);
		}
	}

	this.aPressed=function(event)
	{
	}

	this.update=function()
	{
		if( this.triggerDown ){ this.lineAgent.update(this.editor); }
		else
		if( this.gripDown ){ this.dragAgent.update(this.editor); }

		var tipPosition=this.editor.getControllerTipPosition(this.editor.hands[0]);
        let snapped=this.editor.snap(tipPosition, null, null);
		this.snapVertex.visible=snapped;
		this.snapVertex.position.copy(tipPosition);
	}

}
